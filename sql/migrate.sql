DROP TABLE myTable;
CREATE TABLE myTable_idx(myDate timestamp primary key ,myVal int);
CREATE TABLE myTable(myDate timestamp ,myVal int);


CREATE OR REPLACE FUNCTION initTable(rows int)
RETURNS void as $$
--     DECLARE endDate date := CURRENT_DATE - rows*INTERVAL '1 day';
BEGIN
    delete from myTable
    where 1=1;
    while rows > 0 loop
        insert into myTable(myDate, myVal)
        values (CURRENT_DATE - rows*INTERVAL '1 minute',10);
        insert into myTable_idx(myDate, myVal)
        values (CURRENT_DATE - rows*INTERVAL '1 minute',10);
        rows:=rows-1;
    end loop;
END;
$$ LANGUAGE plpgsql;

select * from public.inittable(1245090);

select 1 from mytable
where myDate = '2020-07-23 07:47:00.000000';
select 1 from myTable_idx
where myDate = '2020-07-23 07:47:00.000000'