package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"time"
)

func main() {

	fmt.Println("Starting ...")
	err := run()
	if err != nil {
		log.Fatalf("app failed: %v", err)
	}
	fmt.Println("Done.")
}

func run() error {
	csvFile, err := os.OpenFile("./csvFile", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return fmt.Errorf("could not openfile: %v", err)
	}
	defer csvFile.Close()

	arr := make([]El, 1e7)
	dic := make(map[time.Time]int)

	err = WriteJson(csvFile, &arr, dic)

	if err != nil {
		return err
	}

	fmt.Println(len(arr))

	return nil
}

type El struct {
	Key time.Time
	Val int
}

func WriteJson(writer io.Writer, arr *[]El, dic map[time.Time]int) error {

	date := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
	today := time.Now()

	ln := int(today.Sub(date).Minutes()) + 1
	//fmt.Printf("Going for a %v steps.\n", ln)

	//a := make([]El,ln )
	*arr = (*arr)[0:ln]
	i := 0

	for date.Before(today) {
		//fmt.Println(date)
		date = date.Add(time.Minute)
		val := i / 12
		//arr=append(arr, El{date,val})
		(*arr)[i] = El{date, val}
		dic[date] = val
		_, err := writer.Write([]byte(fmt.Sprintf("%s, %d\n", date, val)))
		if err != nil {
			log.Fatalf("could not write csv: %v", err)
		}
		i++

		//if math.Mod(float64(i), 1e5) == 0 {
		//
		//	fmt.Printf("At: %f percent \n", float64(i)/float64(ln)*100)
		//}

	}
	return nil

}
