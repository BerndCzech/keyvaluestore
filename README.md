# Data Structure Performance Test

This is a small performance experiment to see what difference SSD Storage / main memory and key value / array has.
The generated set has ~1.2 million lines / 44MB csvFile.

#### AMD Ryzen 7 PRO 3700U w/ Radeon Vega Mobile Gfx 
(Single FLOP tesing is weird - guess it could be a good test offset value: test: 0.3 ns ~3GFlops   , docu: 1792 GFlops)

|       | Main Memory (Golang)      | Hard Disk (SQL) | Redis |
| ----------- | ----------- | ----------- | ----------- | 
|  Slice/no PK/set     | 2.4 ms   | 100ms       | 0.5ms**  |
|  Map/PrimaryKey/Hash-Table    |    27.41 ns (= 0.00002741 ms)| 6 ms        | 0.5ms|

 ** (use redis (unordered) set != redis list)

![img.png](img/img.png)
### SQL
```shell
testdb.public> select 1 from mytable
               where myDate = '2020-07-23 07:47:00.000000'
[2021-05-14 20:42:12] 1 row retrieved starting from 1 in 163 ms (execution: 102 ms, fetching: 61 ms)
testdb.public> select 1 from myTable_idx
               where myDate = '2020-07-23 07:47:00.000000'
[2021-05-14 20:42:12] 1 row retrieved starting from 1 in 237 ms (execution: 6 ms, fetching: 231 ms)
```
### Golang
```shell
bernd@jean-louis ~/go/src/key_value_store % go test -bench . 
goos: linux
goarch: amd64
pkg: key_value_store
cpu: AMD Ryzen 7 PRO 3700U w/ Radeon Vega Mobile Gfx
BenchmarkZero/offset-8                 1        1602336207 ns/op
BenchmarkZero/floating-8        1000000000               0.2778 ns/op
BenchmarkArr/arr-8                   452           2389897 ns/op
BenchmarkDic/dic-8              39736743                27.41 ns/op
PASS
ok      key_value_store 9.261s
go test -bench .  10,69s user 0,74s system 119% cpu 9,548 total
```
### Redis (Docker container, go client)
```shell
bernd@jean-louis ..b.com/BerndCzech/key_value_store/redis (git)-[master] % go test -bench . 
goos: linux
goarch: amd64
pkg: gitlab.com/BerndCzech/key_value_store/redis
cpu: AMD Ryzen 7 PRO 3700U w/ Radeon Vega Mobile Gfx
Benchmark_Data/Array-8              2192            508804 ns/op
Benchmark_Data/Dic-8                2320            495759 ns/op
PASS
ok      gitlab.com/BerndCzech/key_value_store/redis     2.384s
```

## HOW TO

1) Set up docker
2) run sql/migrate.sql
3) ```shell
   go test -bench .
   cd redis
   go test -bench .
   ``