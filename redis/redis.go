package redis

import (
	"fmt"
	"log"
	"math"
	"time"

	// Import the redigo/redis package.
	"github.com/gomodule/redigo/redis"
)

func Arr() (bool, error) {
	// Establish a connection to the Redis server listening on port
	// 6379 of the local machine. 6379 is the default port, so unless
	// you've already changed the Redis configuration file this should
	// work.
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		return false, fmt.Errorf("could not connect %v", err)
	}
	// Importantly, use defer to ensure the connection is always
	// properly closed before exiting the main() function.
	defer conn.Close()

	//Fill(
	arrVal, err := redis.Bool(conn.Do("SISMEMBER", "meko", "2020-05-15 09:38:00 +0000 UTC"))
	if err != nil {
		log.Fatal(err)
	}

	//if arrVal{
	//	fmt.Println("arrVal was there")
	//}
	//	fmt.Println("It was not there")

	return arrVal, nil
}

func Dic() (bool, error) {
	// Establish a connection to the Redis server listening on port
	// 6379 of the local machine. 6379 is the default port, so unless
	// you've already changed the Redis configuration file this should
	// work.
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		return false, fmt.Errorf("could not connect %v", err)
	}
	// Importantly, use defer to ensure the connection is always
	// properly closed before exiting the main() function.
	defer conn.Close()

	dicVal, err := redis.Int(conn.Do("GET", "2020-05-15 09:38:00 +0000 UTC"))
	if err != nil {
		log.Fatal(err)
	}

	//if dicVal != 0{
	//	fmt.Println("dicVal was there")
	//	return nil
	//}
	//fmt.Println("It was not there")

	return dicVal > 0, nil
}

func Fill() error {
	conn, err := redis.Dial("tcp", "localhost:6379")
	if err != nil {
		return fmt.Errorf("could not connect %v", err)
	}
	defer conn.Close()

	date := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
	today := time.Now()

	ln := int(today.Sub(date).Minutes()) + 1
	fmt.Printf("Going for a %v steps.\n", ln)

	i := 0
	err = conn.Send("FLUSHALL")
	if err != nil {
		log.Fatalf("could not clear redis: %v", err)
	}

	err = conn.Send("MULTI")
	if err != nil {
		return fmt.Errorf("could not write redis: %v", err)

	}

	for date.Before(today) {
		//fmt.Println(date)
		date = date.Add(time.Minute)
		//val := i / 12
		if i > 0 && i%100000 == 1 {
			fmt.Printf("At: %v %% \n", math.Round(float64(i)/float64(ln)*100))
		}
		// Send our command across the connection. The first parameter to
		// Do() is always the name of the Redis command (in this example
		// HMSET), optionally followed by any necessary arguments (in this
		// example the key, followed by the various hash fields and values).
		err := conn.Send("SADD", "meko", date)
		if err != nil {
			log.Fatalf("could not write redis: %v", err)
		}
		err = conn.Send("SET", date, 42)
		if err != nil {
			log.Fatalf("could not write redis: %v", err)
		}
		i++

	}

	_, err = conn.Do("EXEC")
	if err != nil {
		return fmt.Errorf("could not write redis: %v", err)

	}

	return nil
}
