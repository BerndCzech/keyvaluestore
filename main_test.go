package main_test

import (
	"bytes"

	"testing"
	"time"

	kvs "gitlab.com/BerndCzech/key_value_store"
)

var (
	result int
	aux    float64
)

//func BenchmarkZero(b *testing.B) {
//	arr := make([]kvs.El, 1e7)
//	dic := make(map[time.Time]int)
//	buf := &byresult inttes.Buffer{}
//	//var a int
//	kvs.WriteJson(buf, &arr, dic)
//	_, _ = time.Parse("2006-01-02 15:04:05 -0700 MST", "2019-01-01 00:13:00 +0000 UTC")
//
//}

func BenchmarkZero(b *testing.B) {
	arr := make([]kvs.El, 1e7)
	dic := make(map[time.Time]int)
	buf := &bytes.Buffer{}
	//var a int
	b.Run("offset", func(b *testing.B) {

		for i := 0; i < b.N; i++ {

			kvs.WriteJson(buf, &arr, dic)
			_, _ = time.Parse("2006-01-02 15:04:05 -0700 MST", "2019-01-01 00:13:00 +0000 UTC")
		}
	})
	b.Run("floating", func(b *testing.B) {

		for i := 0; i < b.N; i++ {

			aux = 3.0 + 4.34

		}
	})

}
func BenchmarkArr(b *testing.B) {
	arr := make([]kvs.El, 1e7)
	dic := make(map[time.Time]int)
	buf := &bytes.Buffer{}
	var a int
	kvs.WriteJson(buf, &arr, dic)
	target, _ := time.Parse("2006-01-02 15:04:05 -0700 MST", "2020-07-23 07:47:00 +0000 UTC")

	b.Run("arr", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			for _, e := range arr {
				if e.Key == target {
					a = e.Val
				}
			}
		}
	},
	)
	//fmt.Printf("Target val: %d \n",a)
	result = a
}

func BenchmarkDic(b *testing.B) {
	arr := make([]kvs.El, 1e7)
	dic := make(map[time.Time]int)
	buf := &bytes.Buffer{}
	var a int
	kvs.WriteJson(buf, &arr, dic)
	target, _ := time.Parse("2006-01-02 15:04:05 -0700 MST", "2020-07-23 07:47:00 +0000 UTC")
	b.Run("dic", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			a = dic[target]
		}
	},
	)
	//fmt.Printf("Target val: %d \n",a)
	result = a
}
